package com.horux.common.mappers

interface IListResponseMapper<T,Rs> : IResponseMapper<List<T>,List<Rs>>