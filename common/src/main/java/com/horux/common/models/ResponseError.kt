package com.horux.common.models

data class ResponseError(
    val error: String
)
