package com.horux.logging.models.enums

enum class DatabaseAction {
        INSERT, UPDATE, DELETE
}
